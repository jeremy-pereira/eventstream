//
//  OwnershipTests.swift
//  EventStreamTests
//
//  Created by Jeremy Pereira on 07/12/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import EventStream

class OwnershipTests: XCTestCase
{

    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testStreamFilterFilter()
    {
        let stream = EventStream.Stream<Int>()
        let expect = expectation(description: "Should get sequence of integers")

        let subscription = stream
            .filter{ $0 > 1 }
            .filter{ $0 < 100 }
            .filter{ $0 % 2 == 0 }
            .subscribe
            {
                switch $0
                {
                case .complete:
                    XCTFail("Should not get complete")
                case .error(let error):
                    XCTFail("\(error)")
                case .next(let payload):
                    XCTAssert(payload > 1  && payload % 2 == 0, "Filter did not work")
					expect.fulfill()
                }
        	}
        stream.send(payload: 4)
        waitForExpectations(timeout: 1)
        {
            if let error = $0
            {
                XCTFail("time out error \(error) on \(subscription)")
            }
        }
    }
}
