//
//  EventStreamTests.swift
//  StompLibTests
//
//  Created by Jeremy Pereira on 29/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox
@testable import EventStream

fileprivate var log = Logger.getLogger("StompLibbTests.EventStreamTests")


class EventStreamTests: XCTestCase
{

    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	func testFilter()
    {
        Logger.pushLevel(.debug, forName: "RealTimeBritishRail.EventStream.Stream")
        defer { Logger.popLevel(forName: "RealTimeBritishRail.EventStream.Stream") }
        Logger.pushLevel(.debug, forName: "RealTimeBritishRail.EventStream.Consumable")
        defer { Logger.popLevel(forName: "RealTimeBritishRail.EventStream.Consumable") }
        let source = EventStream.Stream<Int>()
        let expect = expectation(description: "Should get sequence of integers")

        let dest = source.filter { $0 % 2 == 0 }
        var payloadCount = 0
        let endpoint = dest.subscribe(schedule: .synchronous)
        {
            switch $0
            {
            case .complete:
                expect.fulfill()
            case .error(let err):
                XCTFail("\(err)")
            case .next(let payload):
                payloadCount += 1
                XCTAssert(payload % 2 == 0, "Should only get even numbers")
            }
        }
        for i in 1 ..< 10
        {
            source.send(payload: i)
        }
        source.send(event: .complete)
        waitForExpectations(timeout: 1)
        { (error) in
            if let error = error
            {
                XCTFail("time out error \(error) on \(endpoint)")
            }
        }
        XCTAssert(payloadCount == 4, "Wrong payload count")
    }

    func testSendPayload()
    {
        let testStream = EventStream.Stream<Int>()
        let expect = expectation(description: "Should get an int")

        let subscription = testStream.subscribe {
            switch $0
            {
            case .complete:
                XCTFail("Did not send complete")
            case .error(let error):
                XCTFail("\(error)")
            case .next(let payload):
                XCTAssert(payload == 1, "Expecgted 1, got \(payload)")
                expect.fulfill()
            }
        }
        testStream.send(payload: 1)
        waitForExpectations(timeout: 1)
        {
            if let error = $0
            {
                XCTFail("\(error) on \(subscription)")
            }
        }
    }

    private let sendMessage =
    """
    SEND
    content-length:4
    content-type:text/plain
    destination:/queue/a
    my-header:foo

    body
    """

    func testSubscriber()
    {
//        let stream = EventStream.Stream<[UInt8]>()
//        let expect = expectation(description: "Should get a frame from the frame handler")
//        let parser = FrameParser()
//
//        let subscription = stream.subscribe
//        {
//            switch $0
//            {
//            case .next(let payload):
//                do
//                {
//                    try parser.append(bytes: payload)
//                    {
//                        frame in
//                        XCTAssert(frame.command == .send, "Wrong frame received")
//                        expect.fulfill()
//                    }
//                }
//                catch
//                {
//                    XCTFail("\(error)")
//                }
//            default:
//                XCTFail("\($0)")
//            }
//        }
//
//        stream.send(payload: Array(sendMessage.utf8) + [0])
//
//        waitForExpectations(timeout: 1)
//        {
//            if let error = $0
//            {
//                XCTFail("\(error) on \(subscription)")
//            }
//        }
//
    }

    let messageMessage =
    """
    MESSAGE
    ack:101
    subscription:0
    message-id:007
    destination:/queue/a
    content-type:text/plain

    hello queue a
    """

    func testPipeline()
    {
//        let byteStream = EventStream.Stream<[UInt8]>()
//        let frameParser = FrameParser()
//        let pipe = byteStream.transform(transform: frameParser.append)
//        let expect = expectation(description: "Should get a frame from the frame handler")
//        let pSubs = pipe.subscribe
//        {
//            switch $0
//            {
//            case .next(let payload):
//                XCTAssert(payload.command == .message, "Wrong frame received")
//            default:
//                XCTFail("\($0)")
//            }
//            expect.fulfill()
//        }
//        byteStream.send(payload: Array(messageMessage.utf8) + [0])
//        waitForExpectations(timeout: 1)
//        {
//            if let error = $0
//            {
//                XCTFail("\(error) on \(pSubs)")
//            }
//        }
    }


    func testDoublePipeline()
    {
//        let byteStream = EventStream.Stream<[UInt8]>()
//        let frameParser = FrameParser()
//        let pipe = byteStream.transform(transform: frameParser.append).transform {
//            (frame, callback: (Frame) -> Void) in
//            if frame.command == .message
//            {
//                if let ackValue = frame.ack
//                {
//                    var newFrame = Frame(command: .ack)
//                    newFrame.set(header: .ack(ackValue))
//                    callback(newFrame)
//                }
//            }
//        }
//        let expect = expectation(description: "Should get a frame from the frame handler")
//        let pSubs = pipe.subscribe
//        {
//            switch $0
//            {
//            case .next(let payload):
//                XCTAssert(payload.command == .ack, "Wrong frame received")
//            default:
//                XCTFail("\($0)")
//            }
//            expect.fulfill()
//        }
//        byteStream.send(payload: Array(messageMessage.utf8) + [0])
//        waitForExpectations(timeout: 1)
//        {
//            if let error = $0
//            {
//                XCTFail("\(error) on \(pSubs)")
//            }
//        }
    }

    func testFileDescriptor()
    {
        var fileDescriptors: [Int32] = [0, 0]
        let status = pipe(&fileDescriptors)
        guard status == 0 else { fatalError("Could not create pipe") }
        let readDescriptor = fileDescriptors[0]
        let writeDescriptor = fileDescriptors[1]
        let source = FileDescriptorSource(fileDescriptor: readDescriptor, cleanup: { close(readDescriptor) })
        let expect = expectation(description: "Should get a frame from the frame handler")
		var bytesReceived = 0
        let sSubs = source.subscribe
        {
            print("Got event: \($0)")
            switch $0
            {
            case .complete:
                expect.fulfill()
            case .next(let payload):
				bytesReceived += payload.count
            case .error(let error):
                XCTFail("\(error)")
            }
        }
		source.startReading()
        let testString = messageMessage + "\0"
        testString.withCString
        {
            stringPointer -> Void in
            write(writeDescriptor, stringPointer, testString.utf8.count)
        }
        close(writeDescriptor)
        waitForExpectations(timeout: 1)
        {
            if let error = $0
            {
                XCTFail("\(error) on \(sSubs)")
            }
        }
        XCTAssert(bytesReceived == testString.utf8.count, "Wrong byte count received")
    }

    func testChainFileDescriptorToFrameParser()
    {
//        var fileDescriptors: [Int32] = [0, 0]
//        let status = Darwin.pipe(&fileDescriptors)
//        guard status == 0 else { fatalError("Could not create pipe") }
//        let readDescriptor = fileDescriptors[0]
//        let writeDescriptor = fileDescriptors[1]
//        let source = FileDescriptorSource(fileDescriptor: readDescriptor, cleanup: { close(readDescriptor) })
//        let frameParser = FrameParser()
//        let pipe = source.transform(transform: frameParser.append)
//
//        let expect = expectation(description: "Should get a frame from the frame handler")
//        let pipeEndpoint = pipe.subscribe
//        {
//            switch $0
//            {
//            case .next(let payload):
//                XCTAssert(payload.command == .message, "Wrong frame received")
//            case .complete:
//                expect.fulfill()
//            default:
//                XCTFail("\($0)")
//            }
//        }
//        source.startReading()
//        let testString = messageMessage + "\0"
//        testString.withCString
//            {
//                stringPointer -> Void in
//                write(writeDescriptor, stringPointer, testString.utf8.count)
//        }
//        close(writeDescriptor)
//        waitForExpectations(timeout: 1)
//        {
//            if let error = $0
//            {
//                XCTFail("\(error)")
//            }
//        }
    }

    func testUnixPipe()
    {
        let stringBytes = try! "Hello\n".bytes(encoding: .utf8)
        let data = stringBytes.withUnsafeBytes
        {
            return DispatchData(bytes: $0)
        }

        var fileDescriptors: [Int32] = [0, 0]
        let status = Darwin.pipe(&fileDescriptors)
        guard status == 0 else { fatalError("Could not create pipe") }
        let readDescriptor = fileDescriptors[0]
        let writeDescriptor = fileDescriptors[1]
      	let source = FileDescriptorSource(fileDescriptor: readDescriptor)
        {
            log.debug("Closing read file descriptor")
            close(readDescriptor)
        }
        let writer = FileDescriptorDest(fileDescriptor: writeDescriptor)
        {
            log.debug("Closing write file descriptor")
            close(writeDescriptor)
        }
        defer { writer.close() }
        let pipe = source.map {
            (data: DispatchData) -> String in
            return String(bytes: data, encoding: .utf8)!
        }
        let expectation = XCTestExpectation(description: "Read expectation")
        pipe.subscribe
        {
            switch $0
            {
            case .complete:
                XCTFail("Stream finished before we got the data")
            case .error(let error):
                XCTFail("\(error)")
            case .next(let payload):
                XCTAssert(payload == "Hello\n")
            }
			expectation.fulfill()
        }
        source.startReading()
        writer.write(data: data)
        {
            if let error = $0
            {
                XCTFail("\(error)")
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }


    func testSendFrameToFile()
    {
//        var frame = Frame(command: .connect)
//        frame.set(header: .acceptVersion([DottedNumber([1, 2])]))
//        frame.set(header: .host("localhost"))
//        var fileDescriptors: [Int32] = [0, 0]
//        let status = Darwin.pipe(&fileDescriptors)
//        guard status == 0 else { fatalError("Could not create pipe") }
//        let writeDescriptor = Darwin.open("frame.txt", O_RDWR | O_CREAT | O_TRUNC)
//        guard writeDescriptor != -1 else { fatalError("Could not open file, reason: \(errno)") }
//        guard Darwin.fchmod(writeDescriptor, 0o755) != -1 else { fatalError("Could not chmod file, reason: \(errno)") }
//
//        let dest = FileDescriptorDest(fileDescriptor: writeDescriptor)
//
//        let instream = EventStream.Stream<Frame>()
//        let outStream  = instream
//            .map { frame in try frame.render() }
//            .map { (bytes) in bytes.withUnsafeBytes{ DispatchData(bytes: $0) } }
//        let completeExpect = expectation(description: "Never got the complete")
//           let oSubs = outStream.subscribe
//            {
//                switch $0
//                {
//                case .next(let payload):
//                    log.debug("Writing bytes...")
//                    dest.write(data: payload)
//                    {
//                        (error) in
//                        if let error = error
//                        {
//                            XCTFail("\(error)")
//                        }
//                    }
//                case .complete:
//                    completeExpect.fulfill()
//                case .error(let error):
//                    XCTFail("\(error)")
//                }
//        }
//
//        instream.send(payload: frame)
//        instream.send(event: .complete)
//
//        waitForExpectations(timeout: 5)
//        {
//            (error) in
//            if let error = error
//            {
//                XCTFail("\(error) on \(oSubs)")
//            }
//        }
    }

    func testUnsubscribeStream()
    {
		let inStream = EventStream.Stream<Int>()
        var invocationCount = 0
        let subscription = inStream.subscribe(schedule: .synchronous)
        {
            switch $0
            {
            case .complete:
                break
            case .error(let error):
                XCTFail("\(error)")
            case .next(let payload):
                invocationCount += 1
                XCTAssert(payload == 1, "Invalid payload")
            }
        }

        inStream.send(payload: 1)
        XCTAssert(invocationCount == 1, "Failed to receive event on \(subscription)")
        inStream.unsubscribe(subscriber: subscription)
        inStream.send(payload: 2)
        XCTAssert(invocationCount == 1, "Received event on \(subscription)")
    }

    func testWrapAndSend()
    {
        let unwrappedStream = EventStream.Stream<Int>()
        let inStream = AnyConsumer<Int>.wrap(unwrappedStream)
        var invocationCount = 0
        let subscription = unwrappedStream.subscribe(schedule: .synchronous)
        {
            switch $0
            {
            case .complete:
                break
            case .error(let error):
                XCTFail("\(error)")
            case .next(let payload):
                invocationCount += 1
                XCTAssert(payload == 1, "Invalid payload")
            }
        }
		inStream.send(payload: 1)
        XCTAssert(invocationCount == 1, "send failed to propagate to wrapped stream on \(subscription)")
    }

    class TestConsumer: Consumer
    {
        var lastInt: Int?

        func send(event: Event<Int>)
        {
            switch event
            {
            case .complete:
                break
            case .error(let error):
                XCTFail("\(error)")
            case .next(let payload):
                lastInt = payload
            }
        }
    }

    /// Make sure that when we wrap an AnyConsumer, it returns the same consumer
    /// we had before.
    func testWrapNoRecursion()
    {
        let stream = EventStream.Stream<Int>(name: "test")
        let wrap1 = AnyConsumer<Int>.wrap(stream)
        let wrap2 = AnyConsumer<Int>.wrap(wrap1)
        XCTAssert(wrap1 === wrap2, "Wrap of AnyConsumer really did wrap")

        let wrap3 = AnyConsumable<Int>.wrap(stream)
        let wrap4 = AnyConsumable<Int>.wrap(wrap3)
        XCTAssert(wrap3 === wrap4, "Wrap of AnyConsumable really did wrap")
        XCTAssert(wrap4.name == "test", "Wrong name \(wrap4.name)")
    }

    func testAnyConsumableSubscribe()
    {
		let stream = EventStream.Stream<Int>()
        let wrappedStream = AnyConsumable<Int>.wrap(stream)
        let output = TestConsumer()
        let subscription = wrappedStream.subscribe(schedule: .synchronous, consumer: output)
        stream.send(payload: 1)
		XCTAssert(output.lastInt == 1, "Did not receive the event on \(subscription)")

    }

    func testFlatMap()
    {
        let stream = EventStream.Stream<Int>()
        var receiveCount = 0
        let output = stream
            .flatMap(schedule: .synchronous)
            {
                (input) -> [String] in
                switch input
                {
                case 0: return []
                case 1: return ["X"]
                case 2: return ["X", "X"]
                default: return ["X", "X", "X"]
                }
            }
            .subscribe(schedule: .synchronous)
            {
                switch $0
                {
                case.complete:
                    break
                case .error(let error):
                    XCTFail("\(error)")
                case .next(let payload):
                    receiveCount += 1
                    XCTAssert(payload == "X", "Unexpected payload")
                }
        	}
		stream.send(payload: 0)
        XCTAssert(receiveCount == 0, "Unexpected receiveCount on \(output)")
        stream.send(payload: 1)
        XCTAssert(receiveCount == 1, "Unexpected receiveCount")
        stream.send(payload: 2)
        XCTAssert(receiveCount == 3, "Unexpected receiveCount")
        stream.send(payload: 3)
        XCTAssert(receiveCount == 6, "Unexpected receiveCount")
    }

    func testPipe()
    {
        let inStream = EventStream.Stream<Int>()
        let outStream = EventStream.Stream<Int>()

        _ = inStream.pipe(outStream)
        var receiveCount = 0
        let subscription = outStream.subscribe(schedule: .synchronous)
        {
            switch $0
            {
            case .error(let error):
                XCTFail("\(error)")
            case .complete:
                break
            case .next:
				receiveCount += 1
            }
        }
        inStream.send(payload: 1)
        XCTAssert(receiveCount == 1, "Wrong receive count from \(subscription)")
   }
}
