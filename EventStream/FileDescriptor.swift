//
//  FileDescriptor.swift
//  StompLib
//
//  Created by Jeremy Pereira on 10/10/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Toolbox

fileprivate var log = Logger.getLogger("EventStream.FileDescriptor")


/// A Consumable` that takes reads from a Unix file descriptor and provides
/// the bytes to its subscribers.
open class FileDescriptorSource: Consumable
{
    public typealias Payload = DispatchData
    private let eventStream = Stream<Payload>(name: "fileDescriptorSource.output")
    private let queue: DispatchQueue

    open func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<DispatchData>
        where C : Consumer, C.Payload == DispatchData
    {
        return eventStream.subscribe(schedule: schedule, consumer: consumer)
    }

    open func unsubscribe(subscriber: AnyConsumer<DispatchData>)
    {
        eventStream.unsubscribe(subscriber: subscriber)
    }

    public var name: String { return eventStream.name }

    private let io: DispatchIO

    /// Initialise the source.
    ///
    /// - Parameters:
    ///   - fileDescriptor: readable file descriptor
    ///   - cleanup: Cleanup handler. This is the place to close the file
    ///              descriptor.
    public init(fileDescriptor: Int32, cleanup: @escaping () -> Void = {})
    {
        queue = DispatchQueue(label: "FileDescriptorQueue")
        io = DispatchIO(type: .stream,
                        fileDescriptor: fileDescriptor,
                        queue: queue,
                        cleanupHandler: { _ in cleanup() })
        io.setLimit(lowWater: 1)

    }


    /// How much to attempt to read at once. |Defaults to  4096 bytes. If you
    /// change this after the source has started reading, it won't take effect
    /// until after the next chunk has been read.
    open var readChunkSize: Int = 4096

    /// Start reding the file descriptor. This function should only be called after
    /// setting up the subscribers otherwise bytes might get discarded from the
    /// stream.
    open func startReading()
    {
        io.read(offset: 0,
                length: readChunkSize,
                queue: queue)
        {
            //            [weak self]
            (done, dispatchData, errorCode) in
            //            guard let source = self else { fatalError("Source has gone but the read handler still exists") }
            let source = self
            log.debug("read done=\(done), bytes=\(dispatchData?.count ?? 0), error=\(errorCode)")
            if (!done)
            {
                // Send the data
                guard let dispatchData = dispatchData
                    else { fatalError("Unfinished read with nil data is unexpected") }
                source.sendData(data: dispatchData)
            }
            else if errorCode != 0
            {
                // Error occurred on the channel
                source.eventStream.send(event: .error(EventError.systemError(errorCode)))
            }
            else if let data = dispatchData, data.count != 0
            {
                // done is true, there is no error and the read returned more
                // than zero bytes
                // This read is done, send the bytes and restart the read.
                source.sendData(data: data)
                source.startReading()
            }
            else
            {
                // done is true, there is no error and the data is empty,
                // That means end of file.
                
                source.eventStream.send(event: .complete)
            }
        }
    }

    /// Set the high water mark for reads. Once the read size has reached this
    /// amount of data, it will generate a receive event.
    ///
    /// - Parameter highWater: The new high water mark.
    open func setLimit(highWater: Int)
    {
        io.setLimit(highWater: highWater)
    }

    private func sendData(data: DispatchData)
    {
        // map returns an array. This is a convenient conversion but might be
        // slow.
        eventStream.send(payload: data)
    }

    /// Close this object.
    open func close()
    {
        io.barrier { self.io.close() }
    }
}

/// A destination for a writeable file descriptor. This works like a DispatchIO
/// but hides the details of managing the asynchronous writes. Each instance
/// has a serial disatch queue on which all events happen.
open class FileDescriptorDest
{
    private var queue: DispatchQueue
    private var io: DispatchIO

    /// Initialise the source.
    ///
    /// - Parameters:
    ///   - fileDescriptor: readable file descriptor
    ///   - cleanup: Cleanup handler. This is the place to close the file
    ///              descriptor.
    public init(fileDescriptor: Int32, cleanup: @escaping () -> Void = {})
    {
        queue = DispatchQueue(label: "FileDescriptorQueue")
        io = DispatchIO(type: .stream,
                        fileDescriptor: fileDescriptor,
                        queue: queue,
                        cleanupHandler: { _ in cleanup() })

    }


    /// Write a block of data to the descriptor. On comletion of the write,
    /// the `complete` handler is called, with any werror, if fit occurred.
    ///
    /// - Parameters:
    ///   - data: The data to write to the file descriptor,
    ///   - complete: On completion of the write (or error) this will be called
    ///               The error parameter will be nil, if rhe write was
    ///               entirely successful.
    open func write(data: DispatchData, complete: @escaping (Error?) -> Void)
    {
        log.debug("data count = \(data.count)")
        io.write(offset: 0, data: data, queue: queue)
        {
            (done, remainingData, errorNumber) in
            log.debug("write block: done=\(done),, remaining count=\(remainingData?.count ?? 0), errorNumber=\(errorNumber)")
            if done
            {
                let error: Error? = errorNumber != 0 ? EventError.systemError(errorNumber) : nil
				complete(error)
            }
        }
    }


    /// Close this object.
    open func close()
    {
        io.barrier { self.io.close() }
    }
}
