//
//  EventStream.swift
//  StompLib
//
//  Created by Jeremy Pereira on 29/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Toolbox

private let log = Logger.getLogger("RealTimeBritishRail.EventStream.Stream")


/// A dispatch semaphore based mutex lock
public struct Mutex
{
    private let semaphore = DispatchSemaphore(value: 1)

    public init(){}


    /// Synchronise a block with the mutex. Note that the lock is not recursive.
    ///
    /// - Parameter execute: The block protoected by the mutex
    /// - Returns: The result from the block.
    /// - Throws: if the block throws
    public func sync<Result>(execute: () throws -> Result) rethrows -> Result
    {
        semaphore.wait()
        defer { semaphore.signal() }
        return try execute()
    }
}

/// Strategy for delivering events to subscribers.
///
/// - synchronous: Events are delivered to subscribers synchronously. That is to
///                say the event is delivered to each subscriber in turn on the
///                same thread that the event is created. No guarantees are
///                made as to the order in which subscribers are called, although
///                specific implementations of `Consumable` may make such a
///                guarantee.
/// - asynchroonous: Events are delivered asynchronously. Effectively, the
///                  event is delivered to each subscriber simultaneously.
///                  Implementstions of `Consumable` must ensure that events
///                  are delivered to any specific subscriber in the same order
///                  as they are received.
/// - specificQueue: Events are delivered to subscribers on a specificdispatch
///   				 queue. This is most useful if you want the events to be
///                  processed on the main queue.
///
public enum ScheduleStrategy
{
    case synchronous
    case asynchronous
    case specificQueue(DispatchQueue)
}


/// An event stream is a sequence of events which may or may not have a
/// payload. If they don't have a payload, they are either a complete or error
/// event. Complete and error events always signify the end of the stream.
///
/// Event streams may have one or more producers that add events to the stream
/// and one or more subscribers that consume events from the stream. Events are
/// dispatched to subscribers on a serial queue to guarantee order of delivery.
/// A subscriber is just a function that processes events from the stream
open class Stream<T>: Consumable, Consumer
{
    public typealias Payload = T

    /// Name of the stream for debugging purposes
    public let name: String

    private let mutex = Mutex()

    public init(name: String = "unnamed")
    {
        self.name = name
    }

    private struct SubscriberInfo
    {
        let queue: DispatchQueue?
        var subscription: AnyConsumer<T>

        fileprivate func dispatch(event: Event<T>)
        {
            if let queue = queue
            {
                queue.async
                {
                    self.subscription.send(event: event)
                }
            }
            else
            {
                subscription.send(event: event)
            }
        }
    }

    private var subscribers: [SubscriberInfo] = []

    private var nextSubscriberId = 0

    /// Subscribe to this stream. The stream takes ownership of the subscriber
    ///
    /// - Parameters:
    ///   - schedule: The scheduling strategy for the subscription
    ///   - consumer: The consumer to which events will be sent
    /// - Returns: A subscription with the self as the consumable
    open func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<C.Payload>
        where C : Consumer, T == C.Payload
    {
        return self.subscribeX(schedule: schedule, consumer: consumer, owner: AnyConsumable<T>.wrap(self))
    }


    /// Subscribe to the stream but with a different owner to this stream. This
    /// allows the subscription to own an object that uses this stream to
    /// provide Consumable funtionality.
    ///
    /// - Parameters:
    ///   - schedule: The scheduling strategy for the subscription
    ///   - consumer: The consumer to which events will be sent.
    ///   - owner: The owning consumable.
    /// - Returns: A subscription with the owner as the consumable.
    open func subscribeX<C>(schedule: ScheduleStrategy, consumer: C, owner: AnyConsumable<T>) -> AnyConsumer<C.Payload>
        where C : Consumer, T == C.Payload
    {
        return mutex.sync
        {
            let queue: DispatchQueue?
            switch schedule
            {
            case .asynchronous:
                queue = DispatchQueue(label: "async event queue")
            case .synchronous:
                queue = nil
            case .specificQueue(let specifiedQueue):
                queue = specifiedQueue
            }

            let newSubscription = AnyConsumer<T>.wrap(consumer)
            subscribers.append(SubscriberInfo(queue: queue, subscription: newSubscription))
            return newSubscription
        }
    }


    open func unsubscribe(subscriber: AnyConsumer<T>)
    {
        mutex.sync
        {
            if let removeIndex = subscribers.index(where: {
                return $0.subscription === subscriber
            })
            {
                subscribers.remove(at: removeIndex)
            }
        }
    }

    // MARK: Consumer

    /// Used by producers to send an event
    ///
    /// - Parameter event: The event to send
    open func send(event: Event<Payload>)
    {
        log.debug("\(self): Sending event: \(event)")
        mutex.sync
            {
                // We will do all the asynchronous dispatches before the synchronous
                // ones so that they all appear in parallel with the sync dispatches.
                for subscriber in subscribers.filter({ $0.queue != nil })
                {
                    subscriber.dispatch(event: event)
                }
                for subscriber in subscribers.filter({ $0.queue == nil })
                {
                    subscriber.dispatch(event: event)
                }
                switch event
                {
                case .error, .complete:
                    subscribers.removeAll()
                default:
                    break
                }
        }
    }

}

extension Stream: CustomStringConvertible
{
    public var description: String
    {
        return "\(self.name), \(subscribers.count) subscribers"
    }
}

