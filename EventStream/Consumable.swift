//
//  Consumable.swift
//  EventStream
//
//  Created by Jeremy Pereira on 06/12/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Toolbox

private let log = Logger.getLogger("RealTimeBritishRail.EventStream.Consumable")

/// Event of which the consumable stream is composed.
///
/// - next: Next payload event in the stream
/// - error: An error event
/// - complete: Stream is complete

public enum Event<Payload>
{
    /// The next normal event in the consumable. The payload is the value of the
    /// event.
    case next(Payload)
    /// An error has occurred. This is guaranteed to be the last event sent by
    /// the consumable. The payload can be any Swift error type.
    case error(Swift.Error)
    /// The stream is finished and no more events will be sent.
    case complete
}



/// Protocol  for something that can be consumed. Implementations send events to
/// their subscribers which will conform to `Consumer<Payload>`. An
/// implementation must be able to manage multiple subscribers.
public protocol Consumable
{
    /// The type of object that can be consumed by this consumable.
    associatedtype Payload



    /// Subscribe to this consumable. The return type must be an instance of
    /// `AnyConsumer<Payload>` which can, in turn, be passed to
    /// `unsubscribe(subscriber: )` to unsubscribe the consumer.
    ///
    /// It is assumed that the implementation will take ownership of both the
    /// original consumer and the returned `AnyConsumer`.
    ///
    /// - Parameters:
    ///   - schedule: The schedule on which to dispatch messages.
    ///   - consumer: The consumer to subscribe
    /// - Returns: The consumer wrapped as an AnyConsumer. This can be used to
    ///            unsubscrivbe the consumer at a later date. It also is the
    ///            object that owns the whole chain of streams.
    func subscribe<C: Consumer>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<Payload>
    where C.Payload == Payload

    /// Unsubscribe a subscription
    ///
    /// - Parameter id: The id of the subscription to unsubscribe
    func unsubscribe(subscriber: AnyConsumer<Payload>)

    /// The name of the consumable, mainly useful for logging and diagnostics
    var name: String { get }

}

fileprivate class BlockConsumer<Payload>: Consumer
{
    deinit
    {
        log.debug("deinit of BlockConsumer \(self)")
    }

    let listener: (Event<Payload>) -> ()
    init(listener: @escaping (Event<Payload>) -> ())
    {
        self.listener = listener
    }

    func send(event: Event<Payload>)
    {
        listener(event)
    }

    private var subscriptions: [AnyConsumable<Payload>] = []
}

public extension Consumable
{
    /// Subscribe a closure to this consumable.
    ///
    /// - Parameters:
    ///   - schedule: The scheduling strategy. How events are delivered to
    ///               subscribers. Bydefaul, this is asynchronous
    ///   - listener: The listner function that consumes events from the
    ///               consumable.
    /// - Returns: A consumer object that can be used to unsubscrivbe the listener
    func subscribe(schedule: ScheduleStrategy = .asynchronous,
                   listener: @escaping (Event<Payload>) -> ()) -> AnyConsumer<Payload>
    {
        return self.subscribe(schedule: schedule, consumer: BlockConsumer(listener: listener))
    }

    /// Returns a consumable that creates events of some target type `U` from the
    /// output of the current consumable. If the transform function throws, it
    /// will cause an error event to be sent to subscribers. Transformed values
    /// are sent to subscribers with a callback function.
    ///
    /// - Parameters:
    ///   - name: Name for debugging purposes has a default
    ///   - transform: The function to use for transforming events.
    ///     - `payload`: the payload to convert
    ///     - `callback`: A callback fiunc tion to return transformed values. May
    ///                 be called multiple times - or none.
    ///  - schedule: The scheduling strategy
    /// - Returns: A consumable of the target type.
    public func transform<U>(    name: String = "transform",
                                 schedule: ScheduleStrategy = .asynchronous,
                                 transform: @escaping (Payload, @escaping (U) -> ()) throws -> ()) -> Transform<Payload, U>
    {
        let transformer = Transform<Payload, U>(name: self.name + "=>" + name, transform: transform)
        transformer.subscription = self.subscribe(schedule: schedule, listener: transformer.listen)
        return transformer
    }


    /// Returns a consumable that creates events of some target type `U` from the
    /// output of the current consumable. This consumable takes an event of one
    /// type and produces an event (or events) of a different type.
    ///
    /// - Parameters:
    ///   - name: Name of this consumable for debug purposes
    ///   - schedule: asynchronous or synchronous, default is async
    ///   - transform: The function that will transform the event of one type
    ///     into events of another type.
    /// - Returns: A Consumable that can be subscribed to.
    public func transform<U>(name: String = "transform",
                     	 schedule: ScheduleStrategy = .asynchronous,
                        transform: @escaping (Event<Payload>, @escaping (Event<U>) -> ()) -> ()) -> EventTransform<Payload, U>
    {
        let transformer = EventTransform<Payload, U>(name: self.name + "=>" + name, transform: transform)
        transformer.subscription = self.subscribe(schedule: schedule, listener: transformer.listen)
        return transformer
    }



    /// Returns a consumable that creates events of some target type `U` from the
    /// output of the current consumable. If the transform function throws, it
    /// will cause an error event to be sent to subscribers. Transformed values
    /// are sent to subscribers by returning an array of them. This allows the
    /// mapping of the payload to the target type to be other than 1:1.
    ///
    /// - Parameters:
    ///   - queue: The dispartch queue to run the transform on, if nil, a new
    ///            serial queue is created.
    ///   - transform: function used to transform the payload. Accepts a
    ///                        parasmeter of the type `Payload` and returns an
    ///                        array of transformed values (possibly empty).
    /// - Returns: A consumable of the target type.
    public func flatMap<U>(schedule: ScheduleStrategy = .asynchronous,
                           transform: @escaping (Payload) throws -> [U]) -> Transform<Payload, U>
    {
        return self.transform(name: "flatMap", schedule: schedule)
        {
            (payload, callback) in
            let results = try transform(payload)
            for result in results
            {
                callback(result)
            }
        }
    }


    /// Returns a consumable that creates events of some target type `U` from the
    /// output of this consumable. Use this to transform values on a 1:1 basis.
    ///
    /// if the transform function throws, it will cause an error event to be sent
    /// subscribers.
    ///
    /// - Parameters:
    ///   - schedule: Scheduling for this map operation.
    ///   - transform: The transform function
    /// - Returns: A consumable of type `U`.
    public func map<U>(schedule: ScheduleStrategy = .asynchronous,
                       transform: @escaping (Payload) throws -> U) -> Transform<Payload, U>
    {
        return self.transform(name: "map", schedule: schedule)
        {
            (payload, callback) in
            callback(try transform(payload))
        }
    }


    /// Returns a consumable that creates events of some target type `U` from the
    /// output of this consumable. Use this to transform events on a 1:1 basis.
    ///
    /// This differes from the other `map` in that the transform function gets
    /// events to work on. So you could, for examnple convert a `.complete`
    /// into a `.next` event.
    ///
    /// - Parameters:
    ///   - schedule: Synchronous or asynchronous execution/
    ///   - transform: The event transform. It cannot throw, Instead of throwing
    ///                you are expected to send a `.error` event.
    /// - Returns: A consumable for the transformed events.
    public func map<U>(schedule: ScheduleStrategy = .asynchronous,
                       transform: @escaping (Event<Payload>) -> Event<U>) -> EventTransform<Payload, U>
    {
        return self.transform(name: "map", schedule: schedule)
        {
            (event, callback) in
            callback(transform(event))
        }
    }

    /// Add a filter to the event stream. Events with a payload that matches
    /// the given filter function are passed through, other events are not,
    /// excepting `.error` and `.complete` which cannot be filtered.
    ///
    /// Filters are applied synchronously always.
    ///
    /// - Parameter filterFunc: The function that decides whether to pass a
    ///                         payload on.
    /// - Returns: A new consumable. Subscribers will only see the filtered
    ///            events.
    public func filter(filterFunc: @escaping (Payload) -> Bool) -> Filter<Payload>
    {
        let theFilter = Filter(name: self.name + "=>" + "filter", filter: filterFunc)
        theFilter.subscription = self.subscribe(schedule: .synchronous, listener: theFilter.listen)
        return theFilter
    }

    /// Convenience function to join an existing event stream to this consumable.
    /// Scheduling is always synchronous to avoid over proliferation of queues.
    ///
    /// - Parameter eventStream: The event stream to pipe into
    /// - Returns: the other event stream for chaining pipe calls.
    public func pipe(_ eventStream: Stream<Payload>) -> Stream<Payload>
    {
        _ = self.subscribe(schedule: .synchronous) { eventStream.send(event: $0) }
        return eventStream
    }
}

/// A type that can transform an event stream of one type to an event stream of
/// another type
public class Transform<T, U>: Consumable
{

    public typealias Payload = U

    private var to: Stream<U>
    private let transform: (T, @escaping (U) -> ()) throws -> ()
    public var name: String { return to.name }

    fileprivate var subscription: AnyConsumer<T>?

    /// Initialise a pipe. It requires an event stream as a source and a
    /// function that transfgorms the source events into the target events.
    /// The function must take the payload from a source event anf call a
    /// function passed to it zero or more times with target events in.
    ///
    /// The pipe can be subscribed to exactly as an event stream.
    ///
    /// - Parameters:
    ///   - from: The source event stream
    ///   - transform: A function that transforms a source event to a set of
    ///                target events. For each target event generated, it must
    ///                call the passed in function. If the tramsform throws,
    ///                the target stream will see an error event.
    ///
    public init(name: String = "transform", transform: @escaping (T, @escaping (U) -> ()) throws -> ())
    {
        self.to = Stream<U>(name: name)
        self.transform = transform
    }

    public func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<U> where C : Consumer, U == C.Payload
    {
        return to.subscribeX(schedule: schedule, consumer: consumer, owner: AnyConsumable<U>.wrap(self))
    }

    public func unsubscribe(subscriber: AnyConsumer<U>)
    {
        to.unsubscribe(subscriber: subscriber)
    }

    fileprivate func listen(fromEvent: Event<T>)
    {
        switch fromEvent
        {
        case .error(let e):
            to.send(event: Event<U>.error(e))
        case .complete:
            to.send(event: Event<U>.complete)
        case .next(let payload):
            do
            {
                try transform(payload)
                {
                    transformed in
                    self.to.send(payload: transformed)
                }
            }
            catch
            {
                to.send(event: .error(error))
            }
        }
    }
}


/// A class that  models a filter. It has a closure that "gates" payloads
/// so that subscribers only see filtered events.
public class Filter<T>: Consumable
{
    public typealias Payload = T
    private let to: Stream<T>
    private let filter: (T) -> Bool
    fileprivate var subscription: AnyConsumer<T>?
    public var name: String { return to.name }

    public init(name: String = "filter", filter: @escaping (T) -> Bool)
    {
        to = Stream<T>(name: name)
        self.filter = filter
    }

    deinit
    {
		log.debug("deinit \(self)")
    }

    public func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<T>
        where C : Consumer, T == C.Payload
    {
        return to.subscribeX(schedule: schedule, consumer: consumer, owner: AnyConsumable<T>.wrap(self))
    }

    public func unsubscribe(subscriber: AnyConsumer<T>)
    {
        to.unsubscribe(subscriber: subscriber)
    }

    fileprivate func listen(fromEvent: Event<T>)
    {
        switch fromEvent
        {
        case .error(let e):
            to.send(event: Event<T>.error(e))
        case .complete:
            to.send(event: Event<T>.complete)
        case .next(let payload):
            if filter(payload)
            {
                to.send(event: fromEvent)
            }
        }
    }
}


/// A type that can transform an event stream of one type to an event stream of
/// another type
public class EventTransform<T, U>: Consumable
{
    public typealias Payload = U

    fileprivate var subscription: AnyConsumer<T>?

    private var to: Stream<U>
    private let transform: (Event<T>, @escaping (Event<U>) -> ()) -> ()
    public var name: String { return to.name }

    /// Initialise a pipe. It requires an event stream as a source and a
    /// function that transfgorms the source events into the target events.
    /// The function must take the payload from a source event anf call a
    /// function passed to it zero or more times with target events in.
    ///
    /// The pipe can be subscribed to exactly as an event stream.
    ///
    /// - Parameters:
    ///   - from: The source event stream
    ///   - transform: A function that transforms a source event to a set of
    ///                target events. For each target event generated, it must
    ///                call the passed in function. If the tramsform throws,
    ///                the target stream will see an error event.
    ///
    public init(name: String = "transform", transform: @escaping (Event<T>, @escaping (Event<U>) -> ()) -> ())
    {
        self.to = Stream<U>(name: name)
        self.transform = transform
    }

    public func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<U>
        where C : Consumer, U == C.Payload
    {
        return to.subscribe(schedule: schedule, consumer: consumer)
    }

    public func unsubscribe(subscriber: AnyConsumer<U>)
    {
        to.unsubscribe(subscriber: subscriber)
    }

    fileprivate func listen(_ fromEvent: Event<T>)
    {
        transform(fromEvent)
        {
            (transformed: Event<U>) in
            self.to.send(event: transformed)
        }
    }
}

// MARK: Type erasure gizmo

private class _AnyConsumableBase<Payload>: Consumable
{
    func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<Payload>
        where C : Consumer, _AnyConsumableBase.Payload == C.Payload
    {
        fatalError("subscribe must be overridden")
    }

    func unsubscribe(subscriber: AnyConsumer<Payload>)
    {
        fatalError("unsubscribe must be overridden")
    }

    var name: String
    {
        fatalError("name must be overridden")
    }

    init()
    {
        guard type(of: self) != _AnyConsumableBase.self
        else { fatalError("Cannot instantiate an instance of AnyConsumableBase") }
    }

}

private final class _AnyConsumableBox<Concrete: Consumable>: _AnyConsumableBase<Concrete.Payload>
{
    var concrete: Concrete

    init(_ concrete: Concrete)
    {
        self.concrete = concrete
    }

    override func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<Payload>
        where C : Consumer, Payload == C.Payload
    {
        return concrete.subscribe(schedule: schedule, consumer: consumer)
    }

    override func unsubscribe(subscriber: AnyConsumer<Payload>)
    {
        concrete.unsubscribe(subscriber: subscriber)
    }

    override var name: String
    {
        return concrete.name
    }
}


/// Type erased concrete consumer class.
public final class AnyConsumable<Payload>: Consumable
{
    private let box: _AnyConsumableBase<Payload>

    public init<Concrete: Consumable>(_ concrete: Concrete) where Concrete.Payload == Payload
    {
        box = _AnyConsumableBox(concrete)
    }

    deinit
    {
        log.debug("deinit of AnyConsumable \(self)")
    }

    public func subscribe<C>(schedule: ScheduleStrategy, consumer: C) -> AnyConsumer<Payload>
        where C : Consumer, Payload == C.Payload
    {
        return box.subscribe(schedule: schedule, consumer: consumer)
    }

    public func unsubscribe(subscriber: AnyConsumer<Payload>)
    {
        box.unsubscribe(subscriber: subscriber)
    }

    public var name: String
    {
        return box.name
    }


    /// Wraps a consumable in an AnyConsumable if it is necessary. Instances
    /// that are already `AnyConsumable` do not need wrapping.
    ///
    /// - Parameter consumable: The consumable
    /// - Returns: An `AnyConsumable` that wraps the consumable.
    public static func wrap<T: Consumable>(_ consumable: T) -> AnyConsumable<T.Payload>
    {
        if consumable is AnyConsumable<T.Payload>
        {
			return consumable as! AnyConsumable<T.Payload>
        }
        else
        {
            return AnyConsumable<T.Payload>(consumable)
        }
    }
}
