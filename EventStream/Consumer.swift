//
//  Consumer.swift
//  EventStream
//
//  Created by Jeremy Pereira on 27/11/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

/// A protocol for consumers of events.
public protocol Consumer
{
    associatedtype Payload

    /// Function that the consumable uses to send events to this `Consumer`
    ///
    /// - Parameter event: The event to send.
    func send(event: Event<Payload>)
}

public extension Consumer
{

    /// Convenience function to send a payload without wrapping in a `.next`
    /// event.
    ///
    /// - Parameter payload: The payload to send.
    public func send(payload: Payload)
    {
        self.send(event: .next(payload))
    }
}

// MARK: Type erasure gizmo

private class _AnyConsumerBase<Payload>: Consumer
{
	init()
    {
        guard type(of: self) != _AnyConsumerBase.self
        else { fatalError("Cannot instantiate an inatance of AnyConsumerBase") }
    }

    func send(event: Event<Payload>)
    {
        fatalError("send should be overridden by the subclass")
    }
}

private final class _AnyConsumerBox<Concrete: Consumer>: _AnyConsumerBase<Concrete.Payload>
{
    var concrete: Concrete

    init(_ concrete: Concrete)
    {
        self.concrete = concrete
    }

    override func send(event: Event<Concrete.Payload>)
    {
    	concrete.send(event: event)
    }
}


/// Type erased concrete consumer class.
public final class AnyConsumer<Payload>: Consumer, CustomStringConvertible
{
    private let box: _AnyConsumerBase<Payload>

    public init<Concrete: Consumer>(_ concrete: Concrete) where Concrete.Payload == Payload
    {
        box = _AnyConsumerBox(concrete)
    }

    /// Send an event to the consumer wrapped by thies object
    ///
    /// - Parameter event: Event to semd
    public func send(event: Event<Payload>)
    {
        box.send(event: event)
    }


    /// The description is the same as the description of the wrapped object.
    public var description: String { return "\(box)" }


    /// Wraps an arbitrary object that conforms to `Consumer` with an instance
    /// of `AnyConsumer`. If the object is already an `AnyConsumer`, this
    /// function avoids the double indirection by returning the original object.
    ///
    /// - Parameter consumer: `Consumer` to wrap
    /// - Returns: The wrapped `Consumer`.
    public static func wrap<C: Consumer>(_ consumer: C) -> AnyConsumer<Payload>
    where C.Payload == Payload
    {
        if consumer is AnyConsumer<Payload>
        {
            return consumer as! AnyConsumer<Payload>
        }
        else
        {
            return AnyConsumer(consumer)
        }
    }
}
