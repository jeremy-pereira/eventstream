//
//  EventError.swift
//  EventStream
//
//  Created by Jeremy Pereira on 27/11/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

public enum EventError: Error
{
    case systemError(Int32)
}

