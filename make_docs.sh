#!/bin/sh

author="Jeremy Pereira"
copyright="Copyright © 2016 Jeremy Pereira"

makeADoc() {

	module=$1
	
	echo "Documenting $module"
	
	readme="$module/README.md"
	
	mkdir -p docs/$module
	jazzy -a "$author" -m $module --readme "$readme" --copyright "$copyright" -o "docs/$module" \
		  -x "-target,$module,-scheme,EventStream"
}

if [ x"$1" != "x" ] 
then
	makeADoc "$1"
else
	makeADoc EventStream
fi
